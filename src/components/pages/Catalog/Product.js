import React from 'react';

const Product = (props) => {
    const {
        name,
        manufacturer,
        price,
        image,
        country,
    } = props.data;

    return (
        <div className='product'>
            <div className='wrap'>
                <h4>{name}</h4>
                <img src={image} alt={name} />
                <p>Made: {manufacturer}</p>
                <p>Country: {country}</p>
                <div>
                    <span>{price}$</span>
                    <input
                        value='Add cart'
                        type='button'
                    />
                </div>
            </div>
        </div>
    )

}

export default Product;