import React from 'react';
import ProductsList from './ProductList';
import products from '../../../data/products.json';

import './style.scss';

const Catalog = () => {
    return (
        <div id='catalog' className='page container'>
            <div className='list-product'>
                <ProductsList products={products} />
            </div>
        </div>
    )
}

export default Catalog;