import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Home from './pages/Home';
import Catalog from './pages/Catalog';
import Gallery from './pages/Gallery';
import About from './pages/About';
import Contacts from './pages/Contacts';

const AppRouter = () => {
    return (
        <Switch>
            <Route exact path='/' component={Home} />
            <Route path='/catalog' component={Catalog} />
            <Route path='/gallery' component={Gallery} />
            <Route path='/contacts' component={About} />
            <Route path='/about' component={Contacts} />
        </Switch>
    )
}

export default AppRouter;